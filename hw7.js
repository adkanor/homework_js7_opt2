"use strict";
// 1. Метод forЕach перебирает каждый елемент масива и позволяет применить функцию к каждому елементу масива.
// 2. Задать длинну масива - ноль
// 3.метод Array. isArray(), что бы проверить масив ли это или нет.

let anyArray = [0, "hello", 2345345, {}, false, "world", 23, "23"];

function filterBy(array, typeOfData) {
  let result = array.filter((value) => typeof value !== typeof typeOfData);
  return result;
}

console.log(filterBy(anyArray, ""));
